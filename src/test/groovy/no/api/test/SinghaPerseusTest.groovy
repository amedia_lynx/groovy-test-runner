package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.singha.*
import org.testng.annotations.*

class SinghaPerseusTest extends BaseTest {
    def HydraPage hydraPage;
    def PerseusPage perseusPage;

    @BeforeClass
    void initialize() {
        hydraPage = new HydraPage(driver, wait);
        perseusPage = new PerseusPage(driver, wait);
    }

    @Test(priority = 1, description = 'it should be verify login user')
    void testVerifyUser() {
        hydraPage.initialize();
        hydraPage.performLogin('Singha Proxy', 'Singha');
        perseusPage.initialize();
        perseusPage.verifyUser();
    }  


    @AfterClass
    void tearDown() {
        perseusPage.logout();
    }

}