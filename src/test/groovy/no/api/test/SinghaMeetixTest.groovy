package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.singha.*
import org.testng.annotations.*

class SinghaMeetixTest extends BaseTest {
    def HydraPage hydraPage;
    def MeetixPage meetixPage;

    @BeforeClass
    void initialize() {
        hydraPage = new HydraPage(driver, wait);
        meetixPage = new MeetixPage(driver, wait);
    }

 	@Test(priority = 1, description = 'it should be verify login user')
    void testVerifyUser() {
        hydraPage.initialize();
        hydraPage.performLogin('Singha Proxy', 'Singha');
        meetixPage.initialize();  
        meetixPage.verifyUser();
    }

    @Test(priority = 2, description = 'it should be create netmeeting') 
    void testCreateNetmeeting(){
        meetixPage.selectMenu();
        def rowCount = meetixPage.selectAction();  
        println 'rowCount in main method =' + rowCount;     
        meetixPage.openMeetixForm(); 
        meetixPage.inputMeetixForm(rowCount);
    }

    @Test(priority=3,description='it should be cancel to update netmeeting')
    void testCancelUpdateNetmeeting(){    
        meetixPage.selectMenu();
        def rowCount = meetixPage.selectAction(); 
        meetixPage.chooseNetmeetingAction('update');
        meetixPage.cancelUpdateNetmeeting(rowCount);
    }

    @Test(priority = 4, description= 'it should be update netmeeting')
    void testUpdateNetmeeting(){       
        meetixPage.selectMenu();
        def rowCount = meetixPage.selectAction(); 
        meetixPage.chooseNetmeetingAction('update');
        meetixPage.updateNetmeeting(rowCount);
    }  

    @Test(priority=5, description='it should be add guest to netmeeting') 
    void testAddGuest(){
        meetixPage.selectMenu();
        meetixPage.selectAction(); 
        meetixPage.chooseNetmeetingAction('person');
        meetixPage.addPerson();
    }

    @Test(priority=6,description='it should be update guest')
    void testUpdateGuest(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('person');
        meetixPage.updatePerson();
    }

    @Test(priority=7, description='it should cancel to delete guest')
    void testCancelDeleteGuest(){    
        meetixPage.selectMenu(); 
        meetixPage.selectAction(); 
        meetixPage.chooseNetmeetingAction('person');
        meetixPage.cancelDelPerson();
    }  

    @Test(priority=8, description='it should be cancel to delete netmeeting')
    void testCancelDeleteNetmeeting(){
        meetixPage.selectMenu();
        def rowCount = meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('delete');
        meetixPage.cancelDelNetmeeting(rowCount);
    }

    @Test(priority=9, description='it should be switch from person list to question list')  
    void testSwitchToQuestion(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('person');
        meetixPage.switchToQuestion();
    }

    @Test(priority=10, description='Post question to NetMeeting') 
    void postQuestion(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        def id = meetixPage.getMeetingId();
        meetixPage.postQuestion(id);
    } 

    @Test(priority=11, description='it should be show answer popup')
    void testViewAnswer(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('question');
        meetixPage.showAnswer();
    } 

    @Test(priority=12, description='it should be cancel to delete question')
    void testCancelDeleteQuestion(){
        println 'in test cancel to delete';
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('question');
        def rowCount = meetixPage.countList();
        meetixPage.cancelDelQuestion(rowCount);
    }   

    @Test(priority=13, description='it should be switch from person list to question list')
    void testSwitchToPerson(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('question');
        meetixPage.switchToPerson();
    }

    @Test(priority=14, description='it should be remove one column from listview')
    void testUncheckColumn(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.uncheckColumn();
    }
    
    @Test(priority=15, description='it should be uncheck all columns')
    void testUncheckAll(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.uncheckAll();
    }    
   
   @Test(priority=16, description='it should be delete question')
    void testDeleteQuestion(){
        meetixPage.selectMenu();
        meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('question');        
        def rowCount = meetixPage.countList();
        meetixPage.delQuestion(rowCount);
    }

    @Test(priority=17, description='it should be delete guest')
    void testDeleteGuest(){
        meetixPage.selectMenu(); 
        meetixPage.selectAction();          
        meetixPage.chooseNetmeetingAction('person');
        meetixPage.delPerson();
    }  

    @Test(priority=18, description='it should be delete netmeeting')
    void testDeleteNetmeeting(){
        meetixPage.selectMenu();
        def rowCount = meetixPage.selectAction();
        meetixPage.chooseNetmeetingAction('delete');
        meetixPage.delNetmeeting(rowCount);
    }

    @AfterClass
    void tearDown() {
        meetixPage.logout();
    }

}