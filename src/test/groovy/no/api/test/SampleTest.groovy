package no.api.test

import no.api.test.page.AngularApiPage
import no.api.test.page.GooglePage
import org.testng.annotations.*

import static no.api.test.selenium.ExpectedConditions.*

class SampleTest extends BaseTest {
    def page1;
    def page2;

    @BeforeClass
    void initialize() {
        page1 = new GooglePage(driver, wait);
        page2 = new AngularApiPage(driver, wait);
    }

    @BeforeMethod
    void beforeEach() {

    }

    @AfterMethod
    void afterEach() {

    }

    @Test(expectedExceptions = IndexOutOfBoundsException.class)
    void indexOutOfBoundsAccess() {
        [1,2,3,4].get(4)
    }

    @Test(priority = 1)
    void testQuery() {
        page1.initialize();
        page1.inputQueryString();
    }

    @Test(priority = 2)
    void testAngularPage() {
        page2.initialize();
        page2.inputQueryString();
    }
}

