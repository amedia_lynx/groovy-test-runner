package no.api.test.selenium

import groovy.util.logging.Slf4j
import org.openqa.selenium.Alert
import org.openqa.selenium.NoSuchFrameException
import org.openqa.selenium.StaleElementReferenceException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebDriverException
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.ExpectedCondition

@Slf4j
class ExpectedConditions {

    public static ExpectedCondition<Boolean> angularPresents() {
        return {
            log.debug 'attempting to poll for Angular...';
            def result = it.executeAsyncScript("return clientSideScripts.testForAngular(1, arguments[arguments.length - 1])");
            log.debug 'polling for angular get a result as ' + result;
            return result[0] == true && result[1] == null;
        };
    }

    public static ExpectedCondition<Boolean> angularDoneExecuting() {
        return {
            log.debug 'check for pending HTTP requests...';
            def result1 = it.executeScript("return clientSideScripts.getPendingHttpRequests('html')");
            log.debug 'check for Angular ready...';
            def result2 = it.executeAsyncScript("return clientSideScripts.waitForAngular('html', arguments[arguments.length - 1])");
            log.debug 'pending request array must be empty, and waitForAngular must return null'
            return result1.size == 0 && result2 == null;
        }
    }

    public static ExpectedCondition<Boolean> angularPageReady() {
        return { it.executeAsyncScript("return clientSideScripts.waitForAngular('html', arguments[arguments.length - 1])") == null };
    }

    public static ExpectedCondition<Boolean> noPendingHttpRequests() {
        return {
            log.debug 'attempting to poll for number of pending requests...';
            def result = it.executeScript("return clientSideScripts.getPendingHttpRequests('html')");
            log.debug 'list of pending requests: ' + result + ' with size ' + result.size;
            return result.size == 0;
        };
    }

    public static ExpectedCondition<Boolean> titleIs(final String title) {
        return org.openqa.selenium.support.ui.ExpectedConditions.titleIs(title);
    }

    public static ExpectedCondition<Boolean> titleContains(final String title) {
        return org.openqa.selenium.support.ui.ExpectedConditions.titleContains(title);
    }

    public static ExpectedCondition<WebElement> presenceOfElementLocated(final ByCommand locator) {
        return new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                return findElement(locator, driver);
            }

            @Override
            public String toString() {
                return "presence of element located by: " + locator;
            }
        };
    }

    public static ExpectedCondition<WebElement> visibilityOfElementLocated(final ByCommand locator) {
        return new ExpectedCondition<WebElement>() {
            @Override
            public WebElement apply(WebDriver driver) {
                try {
                    return elementIfVisible(findElement(locator, driver));
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "visibility of element located by " + locator;
            }
        };
    }

    public static ExpectedCondition<List<WebElement>> visibilityOfAllElementsLocatedBy(final ByCommand locator) {
        return new ExpectedCondition<List<WebElement>>() {
            @Override
            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> elements = findElements(locator, driver);
                for(WebElement element : elements){
                    if(!element.isDisplayed()){
                        return null;
                    }
                }
                return elements.size() > 0 ? elements : null;
            }

            @Override
            public String toString() {
                return "visibility of all elements located by " + locator;
            }
        };
    }

    public static ExpectedCondition<List<WebElement>> visibilityOfAllElements(final List<WebElement> elements) {
        return org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfAllElements(elements);
    }

    public static ExpectedCondition<WebElement> visibilityOf(final WebElement element) {
        return org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf(element);
    }

    private static WebElement elementIfVisible(WebElement element) {
        return element.isDisplayed() ? element : null;
    }

    public static ExpectedCondition<List<WebElement>> presenceOfAllElementsLocatedBy(final ByCommand locator) {
        return new ExpectedCondition<List<WebElement>>() {
            @Override
            public List<WebElement> apply(WebDriver driver) {
                List<WebElement> elements = findElements(locator, driver);
                return elements.size() > 0 ? elements : null;
            }

            @Override
            public String toString() {
                return "presence of any elements located by " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> textToBePresentInElement(final WebElement element, final String text) {
        return org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElement(element, text);
    }

    public static ExpectedCondition<Boolean> textToBePresentInElementLocated(final ByCommand locator, final String text) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    String elementText = findElement(locator, driver).getText();
                    return elementText.contains(text);
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("text ('%s') to be present in element found by %s",
                    text, locator);
            }
        };
    }

    public static ExpectedCondition<Boolean> textToBePresentInElementValue(final WebElement element, final String text) {
        return org.openqa.selenium.support.ui.ExpectedConditions.textToBePresentInElementValue(element, text);
    }

    public static ExpectedCondition<Boolean> textToBePresentInElementValue(final ByCommand locator, final String text) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    String elementText = findElement(locator, driver).getAttribute("value");
                    if (elementText != null) {
                        return elementText.contains(text);
                    } else {
                        return false;
                    }
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("text ('%s') to be the value of element located by %s",
                    text, locator);
            }
        };
    }

    public static ExpectedCondition<WebDriver> frameToBeAvailableAndSwitchToIt(final String frameLocator) {
        return org.openqa.selenium.support.ui.ExpectedConditions.frameToBeAvailableAndSwitchToIt(frameLocator);
    }

    public static ExpectedCondition<WebDriver> frameToBeAvailableAndSwitchToIt(final ByCommand locator) {
        return new ExpectedCondition<WebDriver>() {
            @Override
            public WebDriver apply(WebDriver driver) {
                try {
                    return driver.switchTo().frame(findElement(locator, driver));
                } catch (NoSuchFrameException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "frame to be available: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> invisibilityOfElementLocated(final ByCommand locator) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return !(findElement(locator, driver).isDisplayed());
                } catch (NoSuchElementException e) {
                    // Returns true because the element is not present in DOM. The
                    // try block checks if the element is present but is invisible.
                    return true;
                } catch (StaleElementReferenceException e) {
                    // Returns true because stale element reference implies that element
                    // is no longer visible.
                    return true;
                }
            }

            @Override
            public String toString() {
                return "element to no longer be visible: " + locator;
            }
        };
    }

    public static ExpectedCondition<Boolean> invisibilityOfElementWithText(final ByCommand locator, final String text) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    return !findElement(locator, driver).getText().equals(text);
                } catch (NoSuchElementException e) {
                    // Returns true because the element with text is not present in DOM. The
                    // try block checks if the element is present but is invisible.
                    return true;
                } catch (StaleElementReferenceException e) {
                    // Returns true because stale element reference implies that element
                    // is no longer visible.
                    return true;
                }
            }

            @Override
            public String toString() {
                return String.format("element containing '%s' to no longer be visible: %s",
                    text, locator);
            }
        };
    }

    public static ExpectedCondition<WebElement> elementToBeClickable(final ByCommand locator) {
        return new ExpectedCondition<WebElement>() {

            @Override
            public WebElement apply(WebDriver driver) {
                WebElement element = ExpectedConditions.visibilityOfElementLocated(locator).apply(driver);
                try {
                    if (element != null && element.isEnabled()) {
                        return element;
                    } else {
                        return null;
                    }
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return "element to be clickable: " + locator;
            }
        };
    }

    public static ExpectedCondition<WebElement> elementToBeClickable(final WebElement element) {
        return org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable(element);
    }

    public static ExpectedCondition<Boolean> stalenessOf(final WebElement element) {
        return org.openqa.selenium.support.ui.ExpectedConditions.stalenessOf(element);
    }

    public static <T> ExpectedCondition<T> refreshed(final ExpectedCondition<T> condition) {
        return org.openqa.selenium.support.ui.ExpectedConditions.refreshed(condition);
    }

    public static ExpectedCondition<Boolean> elementToBeSelected(final WebElement element) {
        return elementSelectionStateToBe(element, true);
    }

    public static ExpectedCondition<Boolean> elementSelectionStateToBe(final WebElement element, final boolean selected) {
        return org.openqa.selenium.support.ui.ExpectedConditions.elementSelectionStateToBe(element, selected);
    }

    public static ExpectedCondition<Boolean> elementToBeSelected(final ByCommand locator) {
        return elementSelectionStateToBe(locator, true);
    }

    public static ExpectedCondition<Boolean> elementSelectionStateToBe(final ByCommand locator, final boolean selected) {
        return new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                try {
                    WebElement element = driver.findElement(locator);
                    return element.isSelected() == selected;
                } catch (StaleElementReferenceException e) {
                    return null;
                }
            }

            @Override
            public String toString() {
                return String.format("element found by %s to %sbe selected",
                    locator, (selected ? "" : "not "));
            }
        };
    }

    public static ExpectedCondition<Alert> alertIsPresent() {
        return org.openqa.selenium.support.ui.ExpectedConditions.alertIsPresent();
    }

    public static ExpectedCondition<Boolean> not(final ExpectedCondition<?> condition) {
        return org.openqa.selenium.support.ui.ExpectedConditions.not(condition);
    }

    private static WebElement findElement(ByCommand by, WebDriver driver) {
        try {
            if (by.obj instanceof String) {
                return driver.executeScript(by.obj)[0];
            } else {
                return driver.findElement(by.obj);
            }
        } catch (IndexOutOfBoundsException | NoSuchElementException e) {
            throw e;
        } catch (WebDriverException e) {
            log.debug String.format("WebDriverException thrown by findElement(%s)", by);
            throw e;
        }
    }

    private static List<WebElement> findElements(ByCommand by, WebDriver driver) {
        try {
            if (by.obj instanceof String) {
                return driver.executeScript(by.obj);
            } else {
                return driver.findElements(by.obj);
            }
        } catch (WebDriverException e) {
            log.debug String.format("WebDriverException thrown by findElement(%s)", by);
            throw e;
        }
    }
}
