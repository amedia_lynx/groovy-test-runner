package no.api.test.selenium

/**
 *
 * @author boonyasukd
 */
class AngularBy extends By {

    def js(script, String... input) {
        return new ByCommand(String.format(script, (Object[]) input));
    }

    def binding(input) {
        return js("return clientSideScripts.findBindings('%s', %s);", input, 'false');
    }

    def exactBinding(input) {
        return js("return clientSideScripts.findBindings('%s', %s);", input, 'true');
    }

    def model(input) {
        return js("return clientSideScripts.findByModel('%s');", input);
    }

    def buttonText(input) {
        return js("return clientSideScripts.findByButtonText('%s');", input);
    }

    def partialButtonText(input) {
        return js("return clientSideScripts.findByPartialButtonText('%s');", input);
    }

    def repeater(input) {
        return js("return clientSideScripts.findAllRepeaterRows('%s', %s);", input, 'false');
    }

    def exactRepeater(input) {
        return js("return clientSideScripts.findAllRepeaterRows('%s', %s);", input, 'true');
    }

    def cssContainingText(selector, text) {
        return js("return clientSideScripts.findByCssContainingText('%s', '%s');", selector, text);
    }

    def options(input) {
        return js("return clientSideScripts.findByOptions('%s');", input);
    }

    //deepCss <-- probably won't be using this for a while, at least until angular 2.0
}
