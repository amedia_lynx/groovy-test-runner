package no.api.test.selenium

/**
 *
 * @author boonyasukd
 */
class By {

    def className(input) {
        return new ByCommand(org.openqa.selenium.By.className(input));
    }

    def cssSelector(input) {
        return new ByCommand(org.openqa.selenium.By.cssSelector(input));
    }

    def id(input) {
        return new ByCommand(org.openqa.selenium.By.id(input));
    }

    def linkText(input) {
        return new ByCommand(org.openqa.selenium.By.linkText(input));
    }

    def name(input) {
        return new ByCommand(org.openqa.selenium.By.name(input));
    }

    def partialLinkText(input) {
        return new ByCommand(org.openqa.selenium.By.partialLinkText(input));
    }

    def tagName(input) {
        return new ByCommand(org.openqa.selenium.By.tagName(input));
    }

    def xpath(input) {
        return new ByCommand(org.openqa.selenium.By.xpath(input));
    }
}
