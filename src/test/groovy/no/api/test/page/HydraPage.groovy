package no.api.test.page

import org.openqa.selenium.By;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

class HydraPage extends BasePage {
    HydraPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        driver.get("http://localhost:9016/hydra/login.html");
        //driver.get("http://ploenchit.dev.abctech-thailand.com/hydra/login.html");
    }

    void performLogin(appName, expectedTitle) {
        // step 1: click big login button
        driver.findElement(By.className('btn_login')).click();

        if (driver.getCurrentUrl() != 'http://localhost:9016/hydra/index.html') {
       // if (driver.getCurrentUrl() != 'http://ploenchit.dev.abctech-thailand.com/hydra/index.html') {
            // step 2: input email and click next
            wait.until(presenceOfElementLocated(By.id('Email')));
            driver.findElement(By.id('Email')).sendKeys('sombee01@gmail.com');
            driver.findElement(By.id('next')).click();
            

            // step 3: input password and click "sign in"
            wait.until(presenceOfElementLocated(By.id('Passwd')));
            def passwordField = driver.findElement(By.id('Passwd'));
            wait.until(visibilityOf(passwordField));
            passwordField.sendKeys('wanwisa1');
            driver.findElement(By.id('signIn')).click();

            // step 4: approve access --use in localhost
            wait.until(presenceOfElementLocated(By.id('submit_approve_access')));
            def approveButton = driver.findElement(By.id('submit_approve_access'));
            wait.until(elementToBeClickable(approveButton));
            approveButton.click();
            wait.until(titleIs('Welcome to Hydra'));
        }

        // step 5: select app to run
        driver.findElement(By.xpath("//a[contains(., '$appName')]")).click();
        Thread.sleep(5000);

        //--------------------------- Temporary solution due to we romove app selection page -----------------------------------//
        if(appName == "Singha Proxy"){
            def currentURL = driver.getCurrentUrl();
            println 'Current URL ' + driver.getCurrentUrl();
            currentURL = currentURL.replace("singhaclient/app","singhaclient/app/meetix");  //depend on app needed to test in Singha
            //currentURL = currentURL.replace("http://localhost:9016//singhaclient/app","http://localhost:9638/singhaclient/app/meetix");  //depend on app needed to test in Singha
            println 'Replace URL ' + currentURL;
            driver.get(currentURL);
        }else if(appName == "Admapper4"){
            def currentURL = driver.getCurrentUrl();
            println 'Current URL ' + driver.getCurrentUrl();
            currentURL = currentURL.replace("http://localhost:9016//","http://localhost:9088/");
            println 'Replace URL ' + currentURL;
            driver.get(currentURL);
        }else{
            console.println(appName);
        }       
        //---------------------------------------------------------------------------------------------------------------------//

        // step 6: wait until the app page is loaded
        wait.until(titleIs(expectedTitle));
    }
}

