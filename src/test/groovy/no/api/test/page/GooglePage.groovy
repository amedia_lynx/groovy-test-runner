package no.api.test.page

/**
 *
 * @author boonyasukd
 */
class GooglePage extends BasePage {
    GooglePage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        driver.get('http://www.google.com');
    }

    void inputQueryString() {
        Thread.sleep(5000);
        element(by.name('q')).sendKeys('yo!');
        Thread.sleep(5000);
    }
}

