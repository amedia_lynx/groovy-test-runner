package no.api.test.page.odigos

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*

class OdigosAdminPage extends BasePage {
	def username	  = by.xpath('//ul[@class="nav navbar-nav navbar-right"]/li[last()-1]/a');
	def logoutButton  = by.xpath('//a[.="Log out"]');
	def pubButton     = by.cssContainingText('.ng-binding', 'www.tangotidende.no');
	def pubName       = by.xpath('//h1/strong');
	def addBtn		  = by.xpath('//button[contains(.,"Add")]');
	def editBtn       = by.xpath('//tr[last()]/td[last()]/span/button[contains(.,"Edit")]');
	def removeBtn     = by.xpath('//tr[last()]/td[last()]/span/button[contains(.,"Remove")]');
	def sourceInput   = by.id('srcString');
	def destInput	  = by.id('destString');
	def completeMatch = by.cssContainingText('option', 'COMPLETE_MATCH');
	def prefixMatch   = by.cssContainingText('option', 'PREFIX_MATCH');
	def submitBtn	  = by.xpath('//button[.="Submit"]');
	def cancelBtn	  = by.xpath('//button[.="Cancel"]');
	def typeText  	  = by.xpath('//tr[last()]/td[1]/span');
	def sourceText 	  = by.xpath('//tr[last()]/td[2]/span');
	def destText	  = by.xpath('//tr[last()]/td[3]/span');
	def delBox		  = by.xpath('//div[@class="ng-scope"]');
	def delOK         = by.xpath('//div[@class="modal-footer"]/button[.="OK"]');
    def delCancel     = by.xpath('//div[@class="modal-footer"]/button[.="Cancel"]'); 
    def ruleRow 	  = by.repeater('rule in sharedData.currentPublication.rules');
    def toastMessage  = by.className('cg-notify-message');


	OdigosAdminPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void logout() {
        element(logoutButton).click();
        wait.until(titleIs('Welcome to Hydra'));
    }

    void clickEdit(){
        element(editBtn).click();
        wait.until(angularDoneExecuting());    
    }

    void clickSubmit(){
        element(submitBtn).click();
        wait.until(angularDoneExecuting());
    }

    void verifyUsername(){
    	assert element(username).getText() == 'Som Bee';
    }

    void selectPublication(){
        element(pubButton).click();
        wait.until(angularDoneExecuting());
    }

    void verifyPublication(){    	
    	println element(pubName).getText();
    	assert element(pubName).getText() == 'www.tangotidende.no';
    }

    void verifyAddRule(){
        def ruleCount = elements(ruleRow).size();
    	element(pubButton).click();
    	wait.until(angularDoneExecuting());     	
    	element(addBtn).click();
    	wait.until(angularDoneExecuting());
    	element(sourceInput).sendKeys('/sportTest');
    	element(destInput).sendKeys('http://www.tangotidende.no/sport/');
    	element(completeMatch).click();
    	wait.until(angularDoneExecuting());
    	clickSubmit();
    	assert element(typeText).getText()   == 'COMPLETE_MATCH';
    	assert element(sourceText).getText() == '/sportTest';
    	assert element(destText).getText()	 == 'http://www.tangotidende.no/sport/';
        assert elements(ruleRow).size() == ruleCount + 1;
    }

    void verifyCancelRemoveRule(){    	
    	def ruleCount = elements(ruleRow).size();
    	println 'before remove have ' + ruleCount + ' rows';
    	element(removeBtn).click();
    	wait.until(presenceOfElementLocated(delBox));
    	element(delCancel).click();
    	println 'after cancel have ' + elements(ruleRow).size() + ' rows';
    	assert ruleCount == elements(ruleRow).size();
    }

    void verifyUpdatedRule(){
    	def ruleCount = elements(ruleRow).size();
    	element(editBtn).click();
    	element(sourceInput).clear();
    	element(sourceInput).sendKeys('/nyheterTest');
    	element(destInput).clear();
    	element(destInput).sendKeys('http://www.tangotidende.no/nyheter/');
    	element(prefixMatch).click();
    	wait.until(angularDoneExecuting());
    	clickSubmit();
    	assert ruleCount == elements(ruleRow).size();
    	assert element(typeText).getText()   == 'PREFIX_MATCH';
    	assert element(sourceText).getText() == '/nyheterTest';
    	assert element(destText).getText()	 == 'http://www.tangotidende.no/nyheter/';
    }

    void requiredSource(){    	
    	assert element(submitBtn).isEnabled() == true;
    	element(sourceInput).clear();
    	wait.until(angularDoneExecuting());
    	assert element(submitBtn).isEnabled() == false;
    	element(cancelBtn).click();
    }

    void requiredDestination(){    	
    	assert element(submitBtn).isEnabled() == true;
    	element(destInput).clear();
    	wait.until(angularDoneExecuting());
    	assert element(submitBtn).isEnabled() == false;
    	element(cancelBtn).click();
    }

    void requiredType(){    	
    	wait.until(angularDoneExecuting());     	
    	element(addBtn).click();
    	wait.until(angularDoneExecuting());
    	element(sourceInput).sendKeys('/test');
    	element(destInput).sendKeys('http://www.tangotidende.no/');
    	wait.until(angularDoneExecuting());
    	assert element(submitBtn).isEnabled() == false;
    	element(cancelBtn).click();
		wait.until(angularDoneExecuting());
    }

    void checkSourceValidation(){
    	assert element(submitBtn).isEnabled() == true;
    	element(sourceInput).clear();
    	element(sourceInput).sendKeys('12345');
    	wait.until(angularDoneExecuting());
    	assert element(submitBtn).isEnabled() == false;
    	element(cancelBtn).click();
    }

    void checkDestValidation(){
    	assert element(submitBtn).isEnabled() == true;
    	element(destInput).clear();
    	element(destInput).sendKeys('www.tangotidende.com');
    	wait.until(angularDoneExecuting());
    	assert element(submitBtn).isEnabled() == false;
    	element(cancelBtn).click();
    }

    void verifySourcePrefix(){
    	element(prefixMatch).click();
    	element(sourceInput).clear();
    	element(sourceInput).sendKeys('/nyheterTest/');    	
    	wait.until(angularDoneExecuting());
        Thread.sleep(3000);
    	clickSubmit();        
    	assert element(typeText).getText()   == 'PREFIX_MATCH';
    	assert element(sourceText).getText() == '/nyheterTest/';
    }

    void verifySourceComplete(){
    	element(sourceInput).clear();
    	element(sourceInput).sendKeys('/nyheterTest/');
    	element(completeMatch).click();
    	wait.until(angularDoneExecuting());
    	clickSubmit();
    	assert element(typeText).getText()   == 'COMPLETE_MATCH';
    	assert element(sourceText).getText() == '/nyheterTest';
    }

    void verifyToastMessage(){
    	element(sourceInput).clear();
    	element(sourceInput).sendKeys('/sport');
    	element(submitBtn).click();
    	wait.until(presenceOfElementLocated(toastMessage));
    }

    void verifyRemoveRule(){
        def ruleCount = elements(ruleRow).size();
        println 'before remove have ' + ruleCount + ' rows';
        element(removeBtn).click();
        wait.until(presenceOfElementLocated(delBox));
        element(delOK).click();
        println 'after cancel have ' + elements(ruleRow).size() + ' rows';        
        assert elements(ruleRow).size() == ruleCount - 1;
    }
}
