package no.api.test.page.singha

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*

class TestPage extends BasePage {
	def MeetixPage meetixPage;

	

	TestPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void callAppList(){
        element(meetixPage.appList).click();
    }


}