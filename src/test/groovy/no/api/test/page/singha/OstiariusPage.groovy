package no.api.test.page.singha

import no.api.test.page.BasePage
import static no.api.test.selenium.ExpectedConditions.*
import org.openqa.selenium.Keys

class OstiariusPage extends BasePage {
    def googleUserGen   = generatedAccount();
	def username        = by.xpath('//ul[@class="nav navbar-right"]/li[last()-1]');
	def logoutBtn       = by.xpath('//a[contains(.,"Log out")]');
	def addUserMenu		= by.xpath('//a[.="Add User"]');
	def pubMenu  		= by.xpath('//a[.="Publications"]')
	def googleUser		= by.id('username');
	def firstName		= by.id('firstName');
	def lastName		= by.id('lastName');
	def email			= by.id('email');
	def description		= by.id('description');
	def primaryPub 		= by.cssContainingText('option', 'Budstikka');
	def multidesk		= by.name('multideskEnabled');
	def pubAccess		= by.xpath('//input[@role="combobox"]');
    def disabled        = by.name('disabled');
	def submitForm		= by.xpath('//input[@type="submit"]');
    def cancelForm      = by.xpath('//button[contains(.,"Cancel")]');
	def toastMessage 	= by.className('cg-notify-message');
	def pubAction 		= by.xpath('//tbody/tr[./td/span="Budstikka"]//div/button');
	def viewUser		= by.xpath('//tbody/tr[./td/span="Budstikka"]//li/a[.="View Users"]');
	def userAction		= by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]//div/button');
	def editUser		= by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]//li/a["Edit"]');
	def googleUserReq	= by.xpath('//span[@id="usernameStatus"]/following::div'); 
	def firstnameReq	= by.xpath('//span[@id="firstNameStatus"]/following::div'); 
    def lastnameReq     = by.xpath('//span[@id="lastNameStatus"]/following::div');
    def emailReq        = by.xpath('//span[@id="emailStatus"]/following::div');
    def rowFirstname    = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[2]');
    def rowLastname     = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[3]');
    def rowEmail        = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[4]');
    def rowDisabled     = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[9]');    
    def rowPubAccess    = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[7]//button');
    def pubAccessList   = by.repeater('row in obj');
    def closePubList    = by.xpath('//div/button[.="Close"]');
    def rowMultidesk    = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[6]');
    def rowDescription  = by.xpath('//tbody/tr[./td/span="'+ googleUserGen +'"]/td[8]');
    def usersRow        = by.repeater('row in data | orderBy:sortBy:reverse | filter:searchInput');

	OstiariusPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void verifyUser() {
    	assert element(username).getText() == "Som Bee";
    }

    void logout() {
        element(logoutBtn).click();
        wait.until(titleIs('Welcome to Hydra'));
    }

    void selectMenu(menu){
    	if(menu == 'user'){
    		element(addUserMenu).click();
    	}else if(menu == 'pub'){
    		element(pubMenu).click();
    	}        
    }    

    String generatedAccount(){
        def pool = ['a'..'z','A'..'Z',0..9,'_'].flatten()
        Random rand = new Random(System.currentTimeMillis())

        def userChars = (0..4).collect { pool[rand.nextInt(pool.size())] }
        def googleUserGen = userChars.join()
        googleUserGen = 'groovy_' + googleUserGen + '@test.com';        
        return googleUserGen;
    }

    Integer countList(){
        Thread.sleep(2000);
        return elements(usersRow).size();
    }

    void chkRequiredGoogleUser(){
    	element(firstName).sendKeys('test');
    	element(lastName).sendKeys('user');
    	element(email).sendKeys('groovy@test.com');
    	element(description).sendKeys('add new user by groovy');
    	element(primaryPub).click();
    	element(multidesk).click();
    	element(pubAccess).sendKeys('Fremover');
    	element(pubAccess).sendKeys(Keys.ENTER);
    	Thread.sleep(2000);
    	element(submitForm).click();
    	assert element(googleUserReq).getText() == 'Required';
    }

    void chkValidateGoogleUser(){
    	element(googleUser).sendKeys('groovy');
    	element(submitForm).click();
    	assert (element(googleUserReq).getText()).startsWith('String does not match pattern:');
    }

    void chkRequiredFirstName(){
    	element(googleUser).sendKeys('groovy@test.com');
    	element(lastName).sendKeys('user');
    	element(email).sendKeys('groovy@test.com');
    	element(description).sendKeys('add new user by groovy');
    	element(primaryPub).click();
    	element(multidesk).click();
    	element(pubAccess).sendKeys('Fremover');
    	element(pubAccess).sendKeys(Keys.ENTER);
    	Thread.sleep(2000);
    	element(submitForm).click();
    	assert element(firstnameReq).getText() == 'Required';
    }

    void chkRequiredLastName(){
        element(googleUser).sendKeys('groovy@test.com');
        element(firstName).sendKeys('test');
        element(email).sendKeys('groovy@test.com');
        element(description).sendKeys('add new user by groovy');
        element(primaryPub).click();
        element(multidesk).click();
        element(pubAccess).sendKeys('Fremover');
        element(pubAccess).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        element(submitForm).click();
        assert element(lastnameReq).getText() == 'Required';
    }

    void chkRequiredEmail(){
        element(googleUser).sendKeys('groovy@test.com');
        element(firstName).sendKeys('test');
        element(lastName).sendKeys('user');
        element(description).sendKeys('add new user by groovy');
        element(primaryPub).click();
        element(multidesk).click();
        element(pubAccess).sendKeys('Fremover');
        element(pubAccess).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        element(submitForm).click();
        assert element(emailReq).getText() == 'Required';
    }

    void chkValidateEmail(){
        element(email).sendKeys('groovy');
        element(submitForm).click();
        assert (element(emailReq).getText()).startsWith('String does not match pattern:');
    }

    void chkRequiredDescription(){
        element(googleUser).sendKeys('groovy@test.com');
        element(firstName).sendKeys('test');
        element(lastName).sendKeys('user');
        element(email).sendKeys('groovy@test.com');
        element(primaryPub).click();
        element(multidesk).click();
        element(pubAccess).sendKeys('Fremover');
        element(pubAccess).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        element(submitForm).click();
    }

    void chkRequiredPrimaryPub(){
        element(googleUser).sendKeys('groovy@test.com');
        element(firstName).sendKeys('test');
        element(lastName).sendKeys('user');
        element(email).sendKeys('groovy@test.com');
        element(description).sendKeys('add new user by groovy');
        element(multidesk).click();
        element(pubAccess).sendKeys('Fremover');
        element(pubAccess).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        element(submitForm).click();
    }

    void selectPubAction(){
        Thread.sleep(2000);
    	element(pubAction).click();
        Thread.sleep(2000);
    	element(viewUser).click();
        wait.until(angularDoneExecuting());
    }

    void selectEditUser(){
    	element(userAction).click();
    	element(editUser).click();
    }

    void inputUserForm(status){
        println googleUserGen + ' <<<<<< googleUserGen'; 
        element(googleUser).sendKeys(googleUserGen);
        element(firstName).sendKeys('test');
        element(lastName).sendKeys('user');
        element(email).sendKeys('groovy@test.com');
        element(description).sendKeys('add new user by groovy');
        element(primaryPub).click();
        element(multidesk).click();
        element(pubAccess).sendKeys('Fremover');
        element(pubAccess).sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        status == 'add' ?  element(submitForm).click() :  element(cancelForm).click();
        Thread.sleep(2000);
    }

    void updateUserForm(status){
        element(firstName).clear();
        element(firstName).sendKeys('testgroovy');
        element(lastName).clear();
        element(lastName).sendKeys('userupdate');
        element(email).clear();
        element(email).sendKeys('groovyupdate@test.com');
        element(description).clear();
        element(description).sendKeys('update user by groovy');
        element(multidesk).click();
        element(disabled).click();
        element(pubAccess).click();
        element(pubAccess).sendKeys(Keys.DELETE);    
        element(pubAccess).sendKeys('Eikerbladet');
        element(pubAccess).sendKeys(Keys.ENTER);        
        Thread.sleep(2000);
        if(status == 'update'){
            element(submitForm).click();
            Thread.sleep(2000);
            wait.until(presenceOfElementLocated(toastMessage));
        }else{
            element(cancelForm).click() ;
        }
    }

    void assertCancelUpdate(){
        assert element(rowFirstname).getText() == 'test';
        assert element(rowLastname).getText() == 'user';
        assert element(rowEmail).getText() == 'groovy@test.com';
        assert element(rowDescription).getText() == 'add new user by groovy';
        assert element(rowMultidesk).getText() == 'true';
        assert element(rowDisabled).getText() == 'false';     

        element(rowPubAccess).click();
        wait.until(angularDoneExecuting());
        println 'pubAccessList size = ' + elements(pubAccessList).size();
        elements(pubAccessList).eachWithIndex { element, index ->
            println element.getText();
            if(index == 0){
                assert element.getText() == 'aske';
            }else if(index == 1){
                assert element.getText() == 'frem';
            }
        }
        element(closePubList).click();
        wait.until(angularDoneExecuting());        
    }

    void assertUpdate(){
        assert element(rowFirstname).getText() == 'testgroovy';
        assert element(rowLastname).getText() == 'userupdate';
        assert element(rowEmail).getText() == 'groovyupdate@test.com';
        assert element(rowDescription).getText() == 'update user by groovy';
        assert element(rowMultidesk).getText() == 'false';
        assert element(rowDisabled).getText() == 'true';        

        element(rowPubAccess).click();
        wait.until(angularDoneExecuting());
        println 'pubAccessList size = ' + elements(pubAccessList).size();
        elements(pubAccessList).eachWithIndex { element, index ->
            println element.getText();
            if(index == 0){
                assert element.getText() == 'aske';
            }else if(index == 1){
                assert element.getText() == 'eike';
            }
        }
        element(closePubList).click();
        wait.until(angularDoneExecuting());        
    }    

    void assertCount(prevValue){
        assert prevValue == elements(usersRow).size();
    }

}