package no.api.test.page.singha

import groovyx.net.http.RESTClient
import no.api.test.page.BasePage

import static groovyx.net.http.Method.*
import static groovyx.net.http.ContentType.*
import static no.api.test.selenium.ExpectedConditions.*

class MeetixPage extends BasePage {
    def pubListMenu     = by.xpath('//ul/li/a[.="Publication list"]');
	def username        = by.xpath('//ul[@class="nav navbar-right"]/li[last()-1]/a');
	def logoutBtn       = by.xpath('//a[contains(.,"Log out")]');
    def actionBtn       = by.xpath('//tbody/tr[./td[2]/span="www.hadeland.no"]/td[3]/div/button'); 
    def netmeetingList  = by.xpath('//tbody/tr[./td[2]/span="www.hadeland.no"]/td[3]/div//ul/li[1]/a[.="Netmeeting list"]');
    def headerText      = by.xpath('//div/h2');
    def pageAction      = by.xpath('//div/button[contains(.,"Actions")]');
    def createMeeting   = by.xpath('//ul/li/a[contains(.,"Create netmeeting")]');
    def createPerson    = by.xpath('//ul/li/a[contains(.,"Create new person")]');
    def titleBox        = by.id('title');
    def descriptionBox  = by.id('description');
    def createdState    = by.cssContainingText('option', 'CREATED');
    def openQuestState  = by.cssContainingText('option', 'OPEN_FOR_QUESTIONS');
    def openAnsState    = by.cssContainingText('option', 'OPEN_FOR_ANSWERS');
    def notOpenState    = by.cssContainingText('option', 'NOT_OPEN_FOR_QUESTIONS');
    def closedState     = by.cssContainingText('option', 'CLOSED');
    def submitForm      = by.xpath('//div/input[@value="submit form"]');    
    def cancelForm      = by.xpath('//button[contains(.,"Cancel")]');
    def meetixRow       = by.repeater('row in data | orderBy:sortBy:reverse | filter:searchInput'); // meeting & person use the same ng-repeat
    def rowAction       = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//button[@class="btn btn-link dropdown-toggle"]');
    def rowId           = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[1]');
    def rowTitle        = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[3]');
    def rowDescription  = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[4]');
    def rowState        = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[6]');   
    def updateMeeting   = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Update netmeeting"]');
    def deleteMeeting   = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Delete netmeeting"]');
    def questionList    = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Question list"]');
    def personList      = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Person list"]');
    def toastMessage    = by.className('cg-notify-message');
    def closeNotify     = by.xpath('//button[@class="cg-notify-close"]');
    def lastname        = by.id('lastName');
    def firstName       = by.id('firstName');
    def personAction    = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//button[@class="btn btn-link dropdown-toggle"]');
    def personId        = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[1]');
    def personLastname  = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[2]');
    def personFirstname = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]/td[3]');
    def editPerson      = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Edit person"]');
    def deletePerson    = by.xpath('//tbody/tr[./td[3]/span[contains(.,"Groovy")]]//li/a[.="Delete person"]');
    def okBtn           = by.xpath('//div/button[.="OK"]');
    def cancelBtn       = by.xpath('//div/button[.="Cancel"]');
    def closePerson     = by.xpath('//button[@class="btn btn-default" and .="Close"]');
    def siblingDropdown = by.xpath('//div[@id="prevActions"]/button');
    def questionSibling = by.xpath('//ul/li/a[.="Question list"]');
    def personSibling   = by.xpath('//ul/li/a[.="Person list"]');
    def meetingColumn   = by.xpath('//table//th[@class="ng-scope cursor-pointer"]');
    def columnDropdown  = by.xpath('//button[@class="dropdown-toggle ng-binding btn btn-default"]');
    def checkboxHeader  = by.xpath('//div[@class="checkbox"]/label/input');
    def uncheckBtn      = by.xpath('//a[contains(.,"Uncheck All")]');
    def viewAnswer      = by.xpath('//tbody/tr[./td[2]/span[contains(.,"Groovy")]]/td//button[.="view answers"]');
    def popupPane       = by.xpath('//div[@class="modal-dialog modal-lg"]');
    def answerHeading   = by.xpath('//h3[@class="modal-title ng-binding"]');
    def closeAnswer     = by.xpath('//button[.="Close"]');
    def questAction     = by.xpath('//tbody/tr[./td[2]/span[contains(.,"Groovy")]]//button[@class="btn btn-link dropdown-toggle"]');
    def deleteQuest     = by.xpath('//tbody/tr[./td[2]/span[contains(.,"Groovy")]]//li/a[.="Delete question"]');
    
	MeetixPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void selectMenu(){
        element(pubListMenu).click();
    }

	void logout() {
        element(logoutBtn).click();
        wait.until(titleIs('Welcome to Hydra'));
    }

    void verifyUser() {
    	assert element(username).getText() == "Som Bee";
    }

    Integer selectAction(){  
        wait.until(angularDoneExecuting());
        def elem = element(actionBtn);
        driver.executeScript("arguments[0].scrollIntoView(true);", elem);
        driver.executeScript("window.scrollBy(0, -150)");
        elem.click();
        element(netmeetingList).click();
        wait.until(angularDoneExecuting());
        def rowCount = elements(meetixRow).size();
        return rowCount;
    }

    String getMeetingId(){
        def id = element(rowId).getText();
        return id;
    }

    void openMeetixForm(){
        assert element(headerText).getText().startsWith("Netmeeting list for");
        element(pageAction).click();
        element(createMeeting).click();
        wait.until(angularDoneExecuting());
        assert element(headerText).getText() == "Create Netmeeting";
    }

    void inputMeetixForm(rowCount){
        element(titleBox).sendKeys('NetMeeting from Groovy');
        element(descriptionBox).sendKeys('This is Meetix');
        element(createdState).click();
        element(submitForm).click();
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount+1;   
    }

    void chooseNetmeetingAction(meetingAction){      
        element(rowAction).click();
        wait.until(angularDoneExecuting()); 
        if(meetingAction == 'update'){
            element(updateMeeting).click();            
            println 'update netmeeting';
        }else if(meetingAction == 'delete'){
            element(deleteMeeting).click();            
            println 'delete netmeeting';
        }else if(meetingAction == 'question'){
            element(questionList).click();            
            println 'question List';
        }else if(meetingAction == 'person'){
            element(personList).click();
            println 'person List';
        }    
        wait.until(angularDoneExecuting());    
    }

    void cancelUpdateNetmeeting(rowCount){
        element(titleBox).clear();
        element(titleBox).sendKeys('Try Groovy script');
        element(descriptionBox).clear();
        element(descriptionBox).sendKeys('mockdata');
        element(openQuestState).click();        
        element(cancelForm).click();        
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount;
        assert element(rowTitle).getText() != 'Try Groovy script';
        assert element(rowDescription).getText() != 'mockdata'
        assert element(rowState).getText() != ' OPEN_FOR_QUESTIONS';
    }

    void updateNetmeeting(rowCount){
        element(titleBox).clear();
        element(titleBox).sendKeys('Update by Groovy script');
        element(descriptionBox).clear();
        element(descriptionBox).sendKeys('xyzdef');
        element(openAnsState).click();
        element(submitForm).click();      
        wait.until(presenceOfElementLocated(toastMessage));
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount;
        assert element(rowTitle).getText() == 'Update by Groovy script';
        assert element(rowDescription).getText() == 'xyzdef'
        assert element(rowState).getText() == 'OPEN_FOR_ANSWERS';
    } 

    void addPerson(id){
        def personCount = elements(meetixRow).size();
        element(pageAction).click();
        element(createPerson).click();
        wait.until(angularDoneExecuting());
        element(lastname).sendKeys('tester');
        element(firstName).sendKeys('Groovy');
        element(submitForm).click();
        wait.until(presenceOfElementLocated(toastMessage));  
        assert elements(meetixRow).size == personCount + 1;
    }

    void updatePerson(){
        element(personAction).click();
        element(editPerson).click();
        wait.until(angularDoneExecuting());
        element(lastname).clear();
        element(lastname).sendKeys('update');  
        element(firstName).clear();
        element(firstName).sendKeys('Groovyxxx');   
        element(submitForm).click();
        wait.until(presenceOfElementLocated(toastMessage));
        wait.until(angularDoneExecuting());
        assert element(personLastname).getText() == 'update';
        assert element(personFirstname).getText() == 'Groovyxxx';
    }

    void cancelDelPerson(){
        def personCount = elements(meetixRow).size();
        element(personAction).click();
        element(deletePerson).click();
        wait.until(angularDoneExecuting());
        element(cancelBtn).click();
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == personCount;
        Thread.sleep(2000);
    }

    void delPerson(){
        def personCount = elements(meetixRow).size;
        element(personAction).click();
        element(deletePerson).click();
        wait.until(angularDoneExecuting());
        element(okBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == personCount-1;
    }

    void cancelDelNetmeeting(rowCount){
        element(cancelBtn).click();
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount;
    }

    void delNetmeeting(rowCount){
        element(okBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount-1;
    }

    void cancelDelQuestion(rowCount){
        element(questAction).click();
        element(deleteQuest).click();
        element(cancelBtn).click();
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount;
    }

    void delQuestion(rowCount){
        element(questAction).click();
        element(deleteQuest).click();
        element(okBtn).click();
        wait.until(angularDoneExecuting());
        assert elements(meetixRow).size() == rowCount - 1;
    }

    Integer countList(){
        Thread.sleep(2000);
        return elements(meetixRow).size();
    }

    void uncheckColumn(){
        def columnCount = elements(meetingColumn).size;
        println 'columns count before = ' + columnCount;
        element(columnDropdown).click();
        element(checkboxHeader).click();
        println 'columns count after = ' + elements(meetingColumn).size;
        assert elements(meetingColumn).size == columnCount - 1;
    }

    void uncheckAll(){
        def columnCount = elements(meetingColumn).size;
        println columnCount;
        def headList = by.repeater('option in options | filter: searchFilter')
        def headCount = elements(headList).size;
        println "headCount = " + headCount ;
        element(columnDropdown).click();
        element(uncheckBtn).click();
        Thread.sleep(3000);
        assert elements(meetingColumn).size == (columnCount - headCount);
    }

    void switchToQuestion(){
        assert element(headerText).getText().startsWith('Person list for meeting ID');
        element(siblingDropdown).click();
        element(questionSibling).click();
        wait.until(angularDoneExecuting());
        assert element(headerText).getText().startsWith('Question list for meeting ID');
    }

    void switchToPerson(){
        assert element(headerText).getText().startsWith('Question list for meeting ID');
        element(siblingDropdown).click();
        element(personSibling).click();
        wait.until(angularDoneExecuting());
        assert element(headerText).getText().startsWith('Person list for meeting ID');
    }

    void postQuestion(meetingId){
        println 'post question to netmeetingId ' + meetingId;
        //def question = new RESTClient( 'http://ploenchit.dev.abctech-thailand.com/meetix/meeting/v0.1/meeting/'+ meetingId +'/question' );
        def question = new RESTClient( 'http://localhost:9019/meetix/meeting/v0.1/meeting/'+ meetingId +'/question' );
        def resp = question.post(
                        body: [ email : '123@abctech.com', name : 'abc', question: 'Groovy question?'],                            
                        requestContentType: JSON )
        assert resp.status == 200;
    }

    void showAnswer(){
        element(viewAnswer).click();
        wait.until(presenceOfElementLocated(popupPane));
        assert element(answerHeading).getText() == 'ANSWERS' ;
        element(closeAnswer).click();
        Thread.sleep(2000);
    }
}