package no.api.test.page.singha

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*

class OdigosPage extends BasePage {
	def appList          = by.xpath('//li/a[contains(.,"Applications List")]');
	def username         = by.xpath('//ul[@class="nav navbar-nav navbar-right"]/li[last()-1]/a');
	def logoutBtn        = by.xpath('//a[contains(.,"Log out")]');
    def odigosBtn        = by.xpath('//button[contains(.,"odigos")]');
    def actionBtn        = by.xpath('//tbody/tr[./td[1]/span="www.tangotidende.no"]/td/div/button'); 
    def ruleList         = by.xpath('//tbody/tr[./td[1]/span="www.tangotidende.no"]/td/div//ul/li/a[.="view redirect rule list"]');
    def ruleAction       = by.xpath('//div/button[contains(.,"rules Actions")]');
    def createRule       = by.xpath('//ul/li/a[.="create new redirect rule"]');
    def prefixMatch      = by.cssContainingText('option', 'PREFIX_MATCH');
    def completeMatch    = by.cssContainingText('option', 'COMPLETE_MATCH');
    def source           = by.id('source');
    def destination      = by.id('destination');
    def submitBtn        = by.xpath('//input[@type="submit"]');
    def cancelBtn        = by.xpath('//button[contains(.,"Cancel")]');
    def requiredSrcTxt   = by.xpath('//div[../span[@id="sourceStatus"]]');
    def requiredDestTxt  = by.xpath('//div[../span[@id="destinationStatus"]]');
    def requiredTypeTxt  = by.xpath('//div[../select[@name="type"]]');
    def toastMessage     = by.className('cg-notify-message');
    def odigosRow        = by.repeater('row in data | orderBy:sortBy:reverse | filter:searchInput');
    def latestRuleAction = by.xpath('//table//tr[last()]/td[last()]//button');
    def updateRule       = by.xpath('//table//tr[last()]/td[last()]//div//a[contains(.,"update")]');
    def delRule          = by.xpath('//table//tr[last()]/td[last()]//div//a[contains(.,"delete")]');    
    def rowType          = by.xpath('//table//tr[last()]/td[1]/span');
    def rowSource        = by.xpath('//table//tr[last()]/td[2]/span');
    def rowDestination   = by.xpath('//table//tr[last()]/td[3]/span');
    def okDel            = by.xpath('//button[.="OK"]');
    def cancelDel        = by.xpath('//button[.="Cancel"]');
    def odigosColumn     = by.xpath('//div[@class="panel-heading row"]/following::div/div[@class="head-table-fixed ng-scope cursor-pointer"]');    
    def columnDropdown   = by.xpath('//button[@class="dropdown-toggle ng-binding btn btn-default"]');
    def checkboxHeader   = by.xpath('//div[@class="checkbox"]/label/input');
    def uncheckBtn       = by.xpath('//a[contains(.,"Uncheck All")]');

	OdigosPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void logout() {
        element(logoutBtn).click();
        wait.until(titleIs('Welcome to Hydra'));
    }

    void verifyUser() {
        println element(username).getText();
        assert element(username).getText() == "Som Bee";
    }

    void callAppList(){
        element(appList).click();
    }

    void selectPublication(){
        element(odigosBtn).click();
        wait.until(angularDoneExecuting());
        element(actionBtn).click();
        element(ruleList).click();     
        wait.until(angularDoneExecuting()); 
    }

    void openForm(){
        element(ruleAction).click();
        element(createRule).click();
        wait.until(angularDoneExecuting());
    }

    void inputForm(srcTxt,destTxt,type){   
        Thread.sleep(3000);
        if(type == 'P'){
            element(prefixMatch).click();
        }else if(type == 'C'){
            element(completeMatch).click();
        }   
        element(source).sendKeys(srcTxt);
        element(destination).sendKeys(destTxt);
        element(submitBtn).click();      
    }

    void chkRequiredSource(){
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(requiredSrcTxt).getText() == 'Required';
    }

    void chkValidatedSource(){
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(requiredSrcTxt).getText() == 'String does not match pattern: ^/[a-zA-Z0-9]+$';  
    }

    void chkRequiredDestination(){
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(requiredDestTxt).getText() == 'Required';
    }

    void chkValidatedDestination(){
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(requiredDestTxt).getText() == 'String does not match pattern: ^(http|https)://';
    }

    void chkRequiredType(){
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(requiredTypeTxt).getText() == 'Required';
    }

    Integer getOdigosRow(){
        def ruleCount = elements(odigosRow).size();
        println 'rule count = ' + ruleCount;
        return ruleCount;
    }

    void addRule(ruleCount){
        wait.until(presenceOfElementLocated(toastMessage));
        wait.until(angularDoneExecuting()); 
        println 'new list size = ' +  elements(odigosRow).size();
        assert elements(odigosRow).size() == ruleCount+1;
    }

    void selectUpdate(){       
        element(latestRuleAction).click();
        element(updateRule).click();
        clearForm();
    }

    void cancelUpdateRule(){    
        def typeTxt   = element(rowType).getText();
        def sourceTxt = element(rowSource).getText();
        def destTxt   = element(rowDestination).getText();
        selectUpdate();
        element(cancelBtn).click();
        wait.until(angularDoneExecuting());
        assert element(rowType).getText() == typeTxt;
        assert element(rowSource).getText() == sourceTxt;
        assert element(rowDestination).getText() == destTxt;
    }

    void chkUpdateRule(srcTxt,destTxt,type){
        wait.until(angularDoneExecuting());
        if(type == 'P'){
            type = 'PREFIX_MATCH';
        }else if(type == 'C'){
            type = 'COMPLETE_MATCH'
        }   
        assert element(rowType).getText() == type;
        assert element(rowSource).getText() == srcTxt;
        assert element(rowDestination).getText() == destTxt;
    }

    void clearForm(){
        wait.until(angularDoneExecuting());
        element(source).clear();
        element(destination).clear();
    }

    void cancelRemoveRule(ruleCount){
        element(latestRuleAction).click();
        element(delRule).click();
        element(cancelDel).click();
        wait.until(angularDoneExecuting());
        assert elements(odigosRow).size() == ruleCount;
    }    

    void removeRule(ruleCount){
        element(latestRuleAction).click();
        element(delRule).click();
        element(okDel).click();
        Thread.sleep(3000);
        println elements(odigosRow).size();
        assert elements(odigosRow).size() == ruleCount-1;
    }

    void uncheckAll(){
        def columnCount = elements(odigosColumn).size;
        println columnCount;
        def headList = by.repeater('option in options | filter: searchFilter')
        def headCount = elements(headList).size;
        println "<<<<< headCount " + headCount ;
        element(columnDropdown).click();
        element(uncheckBtn).click();
        Thread.sleep(3000);
        println 'elements(odigosColumn).size = ' + elements(odigosColumn).size;
        assert elements(odigosColumn).size == (columnCount - headCount);
    }    

}