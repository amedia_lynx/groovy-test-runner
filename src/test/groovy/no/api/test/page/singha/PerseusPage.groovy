package no.api.test.page.singha

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*

class PerseusPage extends BasePage {
	def username = by.xpath('//ul[@class="nav navbar-nav navbar-right"]/li[last()-1]/a');

	PerseusPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void verifyUser() {
        println element(username).getText();
        assert element(username).getText() == "Som Bee";
    }
}