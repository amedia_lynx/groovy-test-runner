package no.api.test.page

/**
 *
 * @author boonyasukd
 */
class AngularApiPage extends BasePage {
    public AngularApiPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        driver.get('https://docs.angularjs.org/api');
        injectClientScripts();
        wait.until(angularPageReady());
    }

    void inputQueryString() {
        element(by.model('q')).sendKeys('$http');
        wait.until(presenceOfElementLocated(by.model('docs_version')));
        Thread.sleep(3000);
    }
}
