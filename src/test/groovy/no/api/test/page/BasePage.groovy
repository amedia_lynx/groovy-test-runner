package no.api.test.page

import no.api.test.selenium.AngularBy
import no.api.test.selenium.ByCommand
import org.openqa.selenium.support.ui.ExpectedCondition

/**
 *
 * @author boonyasukd
 */
class BasePage {
    protected def driver;
    protected def wait;
    protected def by;
    protected def clientScripts;

    public BasePage(driver, wait) {
        this.driver = driver;
        this.wait = wait;
        by = new AngularBy();
    }

    protected void injectClientScripts() {
        clientScripts = (clientScripts) ? clientScripts : this.getClass().getResource('/clientsidescripts.js').text;
        driver.executeScript(clientScripts);
    }

    protected angularEval(element, expression) {
        return driver.executeScript("return clientSideScripts.evaluate(arguments[0], arguments[1]);", element, expression);
    }

    protected makeElementVisible(ByCommand command) {
        def elem = element(command);
        def setWidthHeight = "arguments[0].style.width = '1px'; arguments[0].style.height = '1px'; ";
        def setVisibility  = "arguments[0].style.visibility = 'visible'; ";
        def setOverflow    = "arguments[0].style.overflow = 'visible'; ";
        def setOpacity     = "arguments[0].style.opacity = 100;";

        driver.executeScript(setWidthHeight + setVisibility + setOverflow + setOpacity, elem);
    }

    protected def element(ByCommand command) {
        return elements(command)[0];
    }

    protected def elements(ByCommand command) {
        return executeCommand(command, true);
    }

    private def executeCommand(ByCommand command, multiple) {
        if (command.obj instanceof String) {
            return driver.executeScript(command.obj);
        } else if (multiple) {
            return driver.findElements(command.obj);
        } else {
            return driver.findElement(command.obj);
        }
    }

    protected void println(def input) {
        System.out.println("  \u2502  ${input}")
    }
}
