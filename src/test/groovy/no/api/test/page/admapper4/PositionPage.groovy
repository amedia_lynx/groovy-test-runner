package no.api.test.page.admapper4

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*


class PositionPage extends BasePage {
	def sizeOption   = by.xpath('//ul[@class="dropdown-menu" and @role="menu"]/li/a[.="600x600"]');
    def posChkBox    = by.xpath('//table/tbody/tr[2]/td[last()-1]/input');
    def posName      = by.xpath('//h1[@class="ng-binding"]');
    def saveBtn      = by.xpath('//button[.="Save"]');
    def toastMessage = by.className('cg-notify-message');
    def cancelBtn    = by.cssContainingText('.btn.btn-default','Cancel');
    def remindBox    = by.xpath('//div[@class="modal-content"]');
    def stayPage     = by.xpath('//button[.="Stay on This Page"]');
    def leavePage    = by.xpath('//button[.="Leave This Page"]');
    def positionMenu = by.xpath('//li/a[.="Positions"]');

    PositionPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {    	        
        injectClientScripts();
        element(positionMenu).click();
    }

    void selectPosAndSize(posName,size){
    	def posButton    = by.cssContainingText('.ng-binding', posName);
    	def sizeButton   = by.buttonText(size);

        println posName +  size;
    	element(posButton).click();   
        Thread.sleep(2000);
    	element(sizeButton).click();
        element(sizeOption).click();
        Thread.sleep(2000);
    }

    void verifyPositionName(selPos){
        def text = element(posName).getText();
        Thread.sleep(2000);
        println ('Position Name is ' + text);
        assert text == selPos;
    }

    void verifyEnableDisableAds(){
        def chkBox = element(posChkBox);
        def val = chkBox.isSelected();
        def expectVal = !val;
        println 'Position checkbox is currently ' + val;

        chkBox.click();
        element(saveBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));
        assert chkBox.isSelected() == expectVal;
    }

    void verifyCancel(){
        def val = element(posChkBox).isSelected();
        element(posChkBox).click();
        element(cancelBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(posChkBox).isSelected() == val;
    }

    void verifyValueWithPublication(pubValue){
        Thread.sleep(1500);
        def val = pubValue.split(',');
        def positionName = by.xpath('//span[@class="ng-binding" and .="'+ val[0] + '"]');
        def sizeList = by.xpath('//button[@class="btn btn-primary dropdown-toggle ng-binding"]');
        def sizeSel = by.xpath('//ul[@class="dropdown-menu" and @role="menu"]/li/a[.="' + val[1] + '"]');
        def posOption = by.xpath('//tr[td="www.firda.no"]/td[last()-1]/input');
        element(positionName).click();
        wait.until(angularDoneExecuting());
        element(sizeList).click();
        element(sizeSel).click();
        wait.until(angularDoneExecuting());
        println 'Checkbox value in position is ' + element(posOption).isSelected();
        assert val[2] == (element(posOption).isSelected()).toString();
    }

    String updatePosition(posName,size){
        def posButton    = by.cssContainingText('.ng-binding', posName);
        def sizeButton   = by.buttonText(size);

        element(posButton).click();
        wait.until(angularDoneExecuting());
        element(sizeButton).click();       
        element(posChkBox).click();
        element(saveBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));

        def val       = element(posChkBox).isSelected();
        def pubName   = element(by.xpath('//tr[2]/td[1]')).getText();
        def pageName  = element(by.xpath('//tr[1]/th[last()-1]/div/span')).getText();
        def returnVal = pubName + ',' + pageName + ',' + posName + ',' + size + ',' + val;
        println 'Value from position is ' + returnVal;
        return returnVal;
    }

    void verifyRemindUser(){        
        def posButton    = by.cssContainingText('.ng-binding', 'Toppbanner');   
        element(posChkBox).click();
        element(posButton).click();
        wait.until(presenceOfElementLocated(remindBox));
        element(stayPage).click();
        wait.until(angularDoneExecuting());
        assert element(posName).getText() == 'Midtbanner 1';
        element(posButton).click();
        element(leavePage).click();
        wait.until(angularDoneExecuting());
        assert element(posName).getText() == "Toppbanner";
    }

}
