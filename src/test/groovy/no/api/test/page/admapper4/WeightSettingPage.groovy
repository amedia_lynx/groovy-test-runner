package no.api.test.page.admapper4

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*


class WeightSettingPage extends BasePage {
    
    def nationalWeight  = by.xpath('//table/tbody/tr[1]/td[last()]/div[last()]/input');
    def localWeight     = by.xpath('//table/tbody/tr[1]/td[1]/div[last()]/input');
    def saveBtn         = by.xpath('//button[.="Save"]'); 
    def toastMessage    = by.className('cg-notify-message');
    def cancelBtn       = by.xpath('//button[.="Cancel"]'); 
    def settingName     = by.xpath('//h1/i');
    def logout          = by.xpath('//a[.="Log out"]');
    def remindBox       = by.xpath('//div[@class="modal-content"]');
    def stayPage        = by.xpath('//button[.="Stay on This Page"]');
    def leavePage       = by.xpath('//button[.="Leave This Page"]');
    def weightedMenu    = by.xpath('//li/a[.="Weighted Distribution Settings"]');

	WeightSettingPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {    	        
        injectClientScripts();
        element(weightedMenu).click();
        wait.until(angularDoneExecuting());
    }

    void adjustingNationalWeight(pubName){
        def pubButton  = by.cssContainingText('.ng-binding', pubName);
    	def weight = Math.floor((Math.random() * 100) + 1);
        println 'Random national weight = ' + weight;

        element(pubButton).click();    	
        wait.until(angularDoneExecuting());
        element(nationalWeight).clear();      
        element(nationalWeight).sendKeys(weight+'');
        element(saveBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));

        def nationalVal = Float.parseFloat(element(nationalWeight).getAttribute('value'));
        def localVal    = Float.parseFloat(element(localWeight).getAttribute('value'));
        assert (nationalVal + localVal) == 100;
    }

    void adjustingLocalWeight(pubName){
        def pubButton  = by.cssContainingText('.ng-binding', pubName);
        def weight = Math.floor((Math.random() * 100) + 1);
        println 'Random local weight = ' + weight;

        element(pubButton).click();     
        wait.until(angularDoneExecuting());
        element(localWeight).clear();      
        element(localWeight).sendKeys(weight+'');
        element(saveBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));

        def nationalVal = Float.parseFloat(element(nationalWeight).getAttribute('value'));
        def localVal    = Float.parseFloat(element(localWeight).getAttribute('value'));
        assert (nationalVal + localVal) == 100;
    }
    
    void verifyCancel(pubName){
        def pubButton  = by.cssContainingText('.ng-binding', pubName);
        def weight = Math.floor((Math.random() * 100) + 1);

        element(pubButton).click();     
        wait.until(angularDoneExecuting());
        def firstValue = element(localWeight).getAttribute('value');
        println 'Before changed weight value is ' + firstValue;
        element(localWeight).clear();      
        element(localWeight).sendKeys(weight+'');
        println 'input weight value is ' + element(localWeight).getAttribute('value') ;
        element(cancelBtn).click();
        Thread.sleep(5000);       

        assert firstValue == element(localWeight).getAttribute('value');
    }

    void verifySettingPubName(pubName){
        def pubButton  = by.cssContainingText('.ng-binding', pubName);
        element(pubButton).click();
        wait.until(angularDoneExecuting());
        println element(settingName).getText();
        assert pubName == element(settingName).getText();
    }

    void verifyRemindUser(){
        def pubButton  = by.cssContainingText('.ng-binding', 'www.ba.no');
        def weight = Math.floor((Math.random() * 100) + 1);

        element(pubButton).click();
        wait.until(angularDoneExecuting());
        element(localWeight).clear();      
        element(localWeight).sendKeys(weight+'');
        pubButton  = by.cssContainingText('.ng-binding', 'www.dt.no');
        element(pubButton).click();
        wait.until(presenceOfElementLocated(remindBox));
        element(stayPage).click();  
        wait.until(angularDoneExecuting()); 
        println element(settingName).getText();
        assert element(settingName).getText() == 'www.ba.no';
        element(pubButton).click();
        wait.until(presenceOfElementLocated(remindBox));
        element(leavePage).click();
        println element(settingName).getText();
        assert element(settingName).getText() == 'www.dt.no';
    }

}