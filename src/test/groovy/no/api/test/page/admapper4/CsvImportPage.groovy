package no.api.test.page.admapper4

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*


class CsvImportPage extends BasePage {
    def csvMenu       = by.xpath('//a[.="CSV Import"]');
    def fileInput     = by.xpath('//input[@type="file"]');
    def toastMessage  = by.className('cg-notify-message');
    def uploadButton  = by.buttonText('Upload');
    def confirmButton = by.buttonText('Confirm'); 
    def errMessage    = by.xpath('//div[@class="cg-notify-message ng-scope alert-danger cg-notify-message-center"]');
    def closeMessage  = by.xpath('//button[@class="cg-notify-close"]');
    def stayPage      = by.xpath('//button[.="Stay on This Page"]');
    def leavePage     = by.xpath('//button[.="Leave This Page"]');
   

    CsvImportPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
        element(csvMenu).click();
        wait.until(angularDoneExecuting());
    }

    void selectFile(filePath) {
        makeElementVisible(fileInput);
        element(fileInput).sendKeys(filePath);
        Thread.sleep(2000);
        element(uploadButton).click();
        wait.until(presenceOfElementLocated(toastMessage));
        element(confirmButton).click();
        wait.until(angularDoneExecuting());
    }

    void importErrFile(filePath){
        makeElementVisible(fileInput);
        element(fileInput).sendKeys(filePath);
        Thread.sleep(1500);
        element(uploadButton).click();
        wait.until(presenceOfElementLocated(errMessage));
        println element(errMessage).getText();
        assert (element(errMessage).getText()).contains('Error uploading');
        element(closeMessage).click();        
    }
}
