package no.api.test.page.admapper4

import no.api.test.page.BasePage

import static no.api.test.selenium.ExpectedConditions.*


class PublicationPage extends BasePage {
    def pageOption   = by.xpath("//ul[@class='dropdown-menu' and @role='menu']/li/a[.='Fotball']");
    def checkbox     = by.xpath('//table/tbody/tr[1]/td[last()]/span[last()]/input');
    def saveButton   = by.cssContainingText('.btn.btn-primary','Save');
    def toastMessage = by.className('cg-notify-message');
    def logoutButton = by.xpath('//a[.="Log out"]');
    def pubName      = by.xpath('//h1[@class="ng-binding"]');
    def cancelBtn    = by.cssContainingText('.btn.btn-default','Cancel');
    def username     = by.xpath('//a[@class="user ng-binding"]');    
    def remindBox    = by.xpath('//div[@class="modal-content"]');
    def stayPage     = by.xpath('//button[.="Stay on This Page"]');
    def leavePage    = by.xpath('//button[.="Leave This Page"]');
    def norwayIcon   = by.xpath('//img[@src="img/Norway-icon.png"]')
    def engIcon      = by.xpath('//img[@src="img/United-kingdom-icon.png"]');
    def menuTitle    = by.xpath('//h3[@class="ng-binding"]');
    def pubMenu      = by.xpath('//li/a[.="Publications"]');


    PublicationPage(driver, wait) {
        super(driver, wait);
    }

    void initialize() {
        injectClientScripts();
        wait.until(angularDoneExecuting());
    }

    void selectMenuPub(){
        element(pubMenu).click();
        wait.until(angularDoneExecuting());
    }

    void selectPubAndPage(pubName, pageName) {
        def pubButton    = by.cssContainingText('.ng-binding', pubName);
        def pageButton   = by.buttonText(pageName);

        element(pubButton).click();
        wait.until(angularDoneExecuting());
        element(pageButton).click();
        element(pageOption).click();
        wait.until(angularDoneExecuting());
    }

    void printPositionList() {
        println '********* POSITION LIST *********';
        elements(by.repeater('data in positionSettings')).eachWithIndex { element, index ->
            println element.getText() + ' at index ' + index;
        }
    }

    void verifySaveFunctionalityWorks() {
        def chkBox = element(checkbox);
        def val = chkBox.isSelected();
        def expectVal = !val;
        println 'value of checkbox is currently ' + val;

        chkBox.click();
        element(saveButton).click();
        wait.until(presenceOfElementLocated(toastMessage));
        assert chkBox.isSelected() == expectVal;
    }

    void logout() {
        element(logoutButton).click();
        wait.until(titleIs('Welcome to Hydra'));
    }

    void verifyPublicationName(selPub){
        def text = element(pubName).getText();
        println('Publication name is ' + text);
        assert text == selPub;    
    }

    void verifyCancel(){       
        def val = element(checkbox).isSelected();       
        element(checkbox).click();
        element(cancelBtn).click();
        wait.until(presenceOfElementLocated(toastMessage));
        assert element(checkbox).isSelected() == val;
    }

    void verifyUserName(){  
        println 'Log in user is ' +  element(username).getText();
        assert element(username).getText() == 'Som Bee';
    }


    String updatedPublication(){
        def positionName = element(by.xpath('//tbody/tr[1]/th')).getText();
        element(checkbox).click();       
        element(saveButton).click();
        wait.until(presenceOfElementLocated(toastMessage));
        def val = element(checkbox).isSelected();       
        def size = element(by.xpath('//tbody/tr[1]/td[last()]/span')).getText();
        def returnVal = positionName + ',' + size + ',' + val;
        println 'Value from publication is ' + returnVal;
        return returnVal;
    }

     void verifyValueWithPosition(posValue){        
        def val = posValue.split(',');
        def pubSel   = by.cssContainingText('.ng-binding', val[0]);
        def pageList = by.xpath('//button[@class="btn btn-primary dropdown-toggle ng-binding"]');
        def pageSel  = by.xpath('//ul[@class="dropdown-menu" and @role="menu"]/li/a[.="' + val[1] + '"]');       
        def chkSel   = by.xpath('//table/tbody/tr[th="'+ val[2] +'"]/td/span[contains(.,"' + val[3] + '")]/input');
        
        element(by.xpath('//a[.="Publications"]')).click();
        wait.until(angularDoneExecuting());
        element(pubSel).click();
        wait.until(angularDoneExecuting());
        element(pageList).click();
        element(pageSel).click();
        wait.until(angularDoneExecuting());
        assert val[4] == (element(chkSel).isSelected()).toString();        
    }

    void changedLanguage(){
        element(norwayIcon).click();
        assert element(menuTitle).getText() == 'PUBLIKASJON';
        element(engIcon).click();
        assert element(menuTitle).getText() == 'PUBLICATION';
    }

    void verifyRemindUser(){
        def pubButton    = by.cssContainingText('.ng-binding', 'www.f-b.no');
        element(checkbox).click();
        element(pubButton).click(); 
        wait.until(presenceOfElementLocated(remindBox));
        element(stayPage).click();  
        wait.until(angularDoneExecuting()); 
        println element(pubName).getText();
        assert element(pubName).getText() == 'www.blv.no';  
        element(pubButton).click();   
        element(leavePage).click();
        wait.until(angularDoneExecuting());
        println element(pubName).getText();
        assert element(pubName).getText() == 'www.f-b.no';
    }

    
}
