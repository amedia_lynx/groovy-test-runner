package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.admapper4.*
import org.testng.annotations.*

class Admapper4Test extends BaseTest {
    def HydraPage hydraPage;
    def PublicationPage pubPage;
    def CsvImportPage csvPage;
    def PositionPage posPage;
    def WeightSettingPage weightPage;

    @BeforeClass
    void initialize() {
        hydraPage = new HydraPage(driver, wait);
        pubPage = new PublicationPage(driver, wait);
        csvPage = new CsvImportPage(driver, wait);
        posPage = new PositionPage(driver, wait);
        weightPage = new WeightSettingPage(driver,wait);
    }

    @Test(priority=1, description='it should be show username in admin page')
    void testVerifyUsername(){
        hydraPage.initialize();
        hydraPage.performLogin('Admapper4', 'Admapper admin');
        pubPage.verifyUserName();
    }

    @Test(priority=2, description='it should be changed language')
    void testInternationalization(){
        pubPage.initialize();
        pubPage.changedLanguage();
    }

    // ********** Publication ************
    @Test(priority=3, description = 'it should be able to correctly disable/enable ads in publication page')
    void testDisableEnableAds() {
        pubPage.selectMenuPub();        
        pubPage.selectPubAndPage('www.ba.no', '1. side');
        pubPage.printPositionList();
        pubPage.verifySaveFunctionalityWorks();
    }  

    @Test(priority=4, description = 'it should be verify publication name')
    void testVerifyPubName(){      
        pubPage.selectMenuPub();
        pubPage.selectPubAndPage('www.lofotposten.no','1. side');
        pubPage.verifyPublicationName('www.lofotposten.no');
    }

    @Test(priority=5, description='it should be verify cancel function in Publication')
    void testVerifyCancelPublication(){
        pubPage.selectMenuPub();
        pubPage.selectPubAndPage('www.amta.no','1. side');
        pubPage.verifyCancel();
    }   

    @Test(priority=6, description='it should be remind user to stay/leave page in Publication')
    void testRemindInPublication(){  
       pubPage.selectMenuPub();
       pubPage.selectPubAndPage('www.blv.no','1. side');
       pubPage.verifyRemindUser();
    }

    @Test(priority = 7, description = 'it should be adjust ads at the same publication')
    void testAdjustSamePublication(){
        pubPage.selectMenuPub();
        pubPage.selectPubAndPage('www.firda.no','1. side');
        def pubValue = pubPage.updatedPublication();        
        posPage.initialize();
        posPage.verifyValueWithPublication(pubValue);
    }

    // ********** CSV ************
    @Test(priority = 8, description='it should be able to upload CSV files')
    void testUploadCsv() {
        csvPage.initialize();
        csvPage.selectFile('/Users/wanwisa/Desktop/Phoebe_files/export_after.csv');
        csvPage.selectFile('/Users/wanwisa/Desktop/Phoebe_files/export_before.csv');
    }

    @Test(priority = 9, description = 'it should be show error message when import incorrect csv') 
    void testVerifyImportError(){
        csvPage.initialize();
        csvPage.importErrFile('/Users/wanwisa/Desktop/Phoebe_files/export_ErrToast5.csv');
    } 

    // ********** Position ************
    @Test(priority = 10, description='it should be verify position name')
    void testVerifyPositionName(){  
        posPage.initialize();
        posPage.selectPosAndSize('Toppbanner','320x64');
        posPage.verifyPositionName('Toppbanner');
    }

    @Test(priority = 11, description='it should be enabled/disabled ads')
    void testEnableInPosition(){
        posPage.initialize();
        posPage.selectPosAndSize('Toppbanner','320x64');
        posPage.verifyEnableDisableAds();
    }

    @Test(priority = 12, description='it should be verify cancel function in Position')
    void testVerifyCancelPosition(){
        posPage.initialize();
        posPage.selectPosAndSize('Bunnbanner','320x64');        
        posPage.verifyCancel();
    }

    @Test(priority=13, description='it should be remind user to stay/leave page in Position')
    void testRemindInPosition(){
        posPage.initialize();
        posPage.selectPosAndSize('Midtbanner 1','600x300');
        posPage.verifyRemindUser();
    }

    @Test(priority = 14, description='it should be adjust ads at the same publication')
    void testAdjustSamePosition(){    
        posPage.initialize();
        def posValue =posPage.updatePosition('Toppbanner','320x64');       
        pubPage.verifyValueWithPosition(posValue);
    }


    // *********** Weight Distribution *********
    @Test(priority = 15, description='it should be adjust national weight')
    void testAdjustNationalWeight(){
        weightPage.initialize();
        weightPage.adjustingNationalWeight('www.ba.no');
    }

    @Test(priority = 16, description='it should be adjust local weight')
    void testAdjustLocalWeight(){        
        weightPage.initialize();
        weightPage.adjustingNationalWeight('www.lofotposten.no');
    }   

    @Test(priority = 17, description='it should be verify cancel function in Weighted Distrbution')
    void testVerifyCancelWeight(){
        weightPage.initialize();
        weightPage.verifyCancel('www.amta.no');
    }

    @Test(priority = 18, description='it should be verify weighted setting publication name')
    void testVerifyWeightPubName(){
        weightPage.initialize();
        weightPage.verifySettingPubName('www.dt.no');
    }

    @Test(priority = 19, description='it should be remind user to stay/leave page in Weighted Distribution')
    void testRemindInWeighted(){
        weightPage.initialize();
        weightPage.verifyRemindUser();
    }

    @AfterClass
    void tearDown() {
        pubPage.logout();
    }
}
