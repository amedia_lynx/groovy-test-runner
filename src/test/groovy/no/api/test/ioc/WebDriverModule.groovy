package no.api.test.ioc

import com.google.inject.AbstractModule;
import groovy.json.JsonSlurper;
import groovy.util.logging.Slf4j
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.firefox.FirefoxBinary
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.selenium.remote.RemoteWebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import java.util.concurrent.TimeUnit

/**
 *
 * @author boonyasukd
 */
@Slf4j
class WebDriverModule extends AbstractModule {
    @Override
    protected void configure() {
        def driver;
        def wait;
        def config = new JsonSlurper().parseText(this.getClass().getResource('/config.json').text);
        System.setProperty("webdriver.chrome.driver", config.chromeDriver);

        if (config.directConnect) {
            switch(config.capabilities.browserName) {
            case "chrome":
                driver = new ChromeDriver();
                break;
            case "firefox":
                if (config.firefoxPath) {
                    driver = new FirefoxDriver(new FirefoxBinary(new File(config.firefoxPath)), null);
                } else {
                    driver = new FirefoxDriver();
                }
                break;
            }
        } else {
            def capabilities = config.capabilities as DesiredCapabilities;
            log.debug capabilities;
            driver = new RemoteWebDriver(config.seleniumAddress.toURL(), capabilities);
        }

        wait = new WebDriverWait(driver, config.testTimeout, config.sleepMsec);
        driver.manage().timeouts().setScriptTimeout(config.scriptTimeout, TimeUnit.SECONDS);

        log.debug 'eager singletons'
        // EAGER_SINGLETON for both types
        bind(WebDriver.class).toInstance(driver);
        bind(WebDriverWait.class).toInstance(wait);
    }
}

