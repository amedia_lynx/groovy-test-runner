package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.singha.*
import org.testng.annotations.*

class SinghaOstiariusTest extends BaseTest {
    def HydraPage hydraPage;
    def OstiariusPage ostiaPage;

    @BeforeClass
    void initialize() {
        hydraPage = new HydraPage(driver, wait);
        ostiaPage = new OstiariusPage(driver, wait);
    }

    @Test(priority = 1, description = 'it should be verify login user')
    void testVerifyUser() {
        hydraPage.initialize();
        hydraPage.performLogin('Ostiarius', 'Singha');
        ostiaPage.initialize();  
        ostiaPage.verifyUser();
    }

    @Test(priority=2, description='it should be required Google Username')
    void testRequiredGoogleUser(){	
    	ostiaPage.selectMenu('user');
    	ostiaPage.chkRequiredGoogleUser();
    }

    @Test(priority=3, description='it should be validate Google Username')
    void testValidateGoogleUser(){	
    	ostiaPage.selectMenu('user');
    	ostiaPage.chkValidateGoogleUser();
    }

    @Test(priority=4, description='it should be required First name')
    void testRequiredFirstName(){	
    	ostiaPage.selectMenu('user');
    	ostiaPage.chkRequiredFirstName();
    }

    @Test(priority=5, description='it should be required Last name')
    void testRequiredศฟหะName(){   
        ostiaPage.selectMenu('user');
        ostiaPage.chkRequiredLastName();
    }

    @Test(priority=6, description ='it should be required Email')
    void testRequiredEmail(){
        ostiaPage.selectMenu('user');
        ostiaPage.chkRequiredEmail();
    }

    @Test(priority=7,description='it should be validate Email')
    void testValidateEmail(){
        ostiaPage.selectMenu('user');
        ostiaPage.chkValidateEmail();
    }

    @Test(priority=8, description='it should be required Description')
    void testRequiredDescription(){
        ostiaPage.selectMenu('user');
        ostiaPage.chkRequiredDescription();
    }

    @Test(priority=9, description='it should be required Primary publication')
    void testRequiredPrimaryPub(){
        ostiaPage.selectMenu('user');
        ostiaPage.chkRequiredPrimaryPub()
    }

    @Test(priority=10, description='it should be cancel to add new user')
    void testCancelAddUser(){
        ostiaPage.selectMenu('pub');
        ostiaPage.selectPubAction();
        def usersCount = ostiaPage.countList();
        ostiaPage.selectMenu('user');
        ostiaPage.inputUserForm('cancel');
        ostiaPage.selectMenu('pub');
        ostiaPage.selectPubAction();
        ostiaPage.assertCount(usersCount);
    }
    
    @Test(priority=11, description='it should be add new user')                                 
    void testAddUser(){       
        ostiaPage.selectMenu('user');
        ostiaPage.inputUserForm('add');
    }

    @Test(priority=12,description='it should be cancel to update user')
    void testCancelUpdateUser(){
        ostiaPage.selectMenu('pub');
        ostiaPage.selectPubAction();
        ostiaPage.selectEditUser();
        ostiaPage.updateUserForm('cancel');
        ostiaPage.assertCancelUpdate();
    }

    @Test(priority=13 , description='it should be update user')
    void testUpdateUser(){
    	ostiaPage.selectMenu('pub');
    	ostiaPage.selectPubAction();
    	ostiaPage.selectEditUser();
        ostiaPage.updateUserForm('update');
        ostiaPage.assertUpdate();
    }

    @AfterClass
    void tearDown() {
        ostiaPage.logout();
    }
}