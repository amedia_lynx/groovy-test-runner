package no.api.test

import com.google.inject.Inject
import groovy.util.logging.Slf4j
import no.api.test.ioc.WebDriverModule
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait
import org.testng.annotations.AfterClass
import org.testng.annotations.AfterSuite
import org.testng.annotations.BeforeClass
import org.testng.annotations.Guice

@Slf4j
@Guice(modules = WebDriverModule.class)
abstract class BaseTest {

    @Inject
    protected WebDriver driver;

    @Inject
    protected WebDriverWait wait;

    @BeforeClass
    void beforeClass() {

    }

    @AfterClass
    void afterClass() {
        driver.executeScript('window.open()');        
        log.debug 'closing webdriver...'
        driver.close();
        driver.switchTo().window(driver.windowHandles.iterator().next());
    }

    @AfterSuite
    void afterSuite() {
        log.debug 'quitting webdriver...'
        driver.quit();
    }

    protected void println(def input) {
        System.out.println("  \u2502  ${input}")
    }
}
