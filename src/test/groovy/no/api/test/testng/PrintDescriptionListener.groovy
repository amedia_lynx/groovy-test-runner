package no.api.test.testng

import org.testng.IInvokedMethod
import org.testng.IInvokedMethodListener
import org.testng.ITestResult

import static org.fusesource.jansi.Ansi.*
import static org.fusesource.jansi.Ansi.Attribute.*
import static org.fusesource.jansi.Ansi.Color.*

/**
 *
 * @author boonyasukd
 */
class PrintDescriptionListener implements IInvokedMethodListener {
    private static final def CHECK = '\u2714'
    private static final def CROSS = '\u2718'
    private static final def SIDEBAR = '  \u2502  ';

    void afterInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            def result = (method.getTestResult().isSuccess()) ? CHECK : CROSS
            def line = result + '  ' + method.getTestMethod().getDescription()

            println SIDEBAR
            if (result == CHECK) {
                println(SIDEBAR + ansi().fg(GREEN).bold().a(line).reset())
            } else {
                println(SIDEBAR + ansi().fg(RED).bold().a(line).reset())
            }

            println('  \u2558' + ''.padRight(line.length() + 4, '\u2550') + '\n')
        }
    }

    void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        if (method.isTestMethod()) {
            println SIDEBAR
        }
    }
}
