package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.odigos.*
import org.testng.annotations.*

class OdigosTest extends BaseTest {
	def HydraPage hydraPage;
	def OdigosAdminPage odigosPage;

	@BeforeClass
    void initialize() {
        hydraPage  = new HydraPage(driver, wait);   
        odigosPage = new OdigosAdminPage(driver, wait);    
    }

    @Test(priority=1,description='it should be verify publication name')
    void testVerifyPublication(){
    	hydraPage.initialize();
        hydraPage.performLogin('Odigos', 'Odigos Admin');
        odigosPage.initialize();
        odigosPage.selectPublication();
        odigosPage.verifyPublication();
    }

    @Test(priority=2,description='it should be add new rule')
    void testAddRule(){
        odigosPage.selectPublication();
    	odigosPage.verifyAddRule();
    }

    @Test(priority=3,description='it should be cancel to remove rule')
    void cancelToRemoveRule(){
        odigosPage.selectPublication();
    	odigosPage.verifyCancelRemoveRule();
    }

    @Test(priority=4,description='it should be updated rule')
    void testUpdatedRule(){
        odigosPage.selectPublication();
    	odigosPage.verifyUpdatedRule();
    }

    @Test(priority=5,description='it should be verify username')
    void testVerifyUsername(){
    	odigosPage.verifyUsername();
    }

    @Test(priority=6, description='it should be required source')
    void testRequiredSource(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
    	odigosPage.requiredSource();
    }

    @Test(priority=7, description='it should be required destination')
    void testRequiredDestination(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
    	odigosPage.requiredDestination();
    }

    @Test(priority=8,description='it should be required type')
    void testRequiredType(){
        odigosPage.selectPublication();
    	odigosPage.requiredType();
    }

    @Test(priority=9,description='it should be check source validation')
    void testSourceValidation(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
    	odigosPage.checkSourceValidation();
    }

    @Test(priority=10,description='it should be check destination validation')
    void testDestValidation(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
    	odigosPage.checkDestValidation();
    }

    @Test(priority=11,description='it should be verify source in case of type is PREFIX_MATCH')
    void testSourcePrefixMatch(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
        odigosPage.verifySourcePrefix();
    }


    @Test(priority=12,description='it should be verify source in case of type is COMPLETE_MATCH')
    void testSourceCompleteMatch(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
        odigosPage.verifySourceComplete();
    }

    @Test(priority=13,description='it should be show toast notification')
    void testToastNotification(){
        odigosPage.selectPublication();
        odigosPage.clickEdit();
        odigosPage.verifyToastMessage();
    }

    @Test(priority=14,description='it should be removed rule')
    void testRemovedRule(){
        odigosPage.selectPublication();
        odigosPage.verifyRemoveRule();
    }

    @AfterClass
    void tearDown() {    	
        odigosPage.logout();
    }

}