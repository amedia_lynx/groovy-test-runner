package no.api.test

import no.api.test.page.HydraPage
import no.api.test.page.singha.*
import org.testng.annotations.*

class SinghaOdigosTest extends BaseTest {
    def HydraPage hydraPage;
    def OdigosPage odigosPage;

    @BeforeClass
    void initialize() {
        hydraPage = new HydraPage(driver, wait);
        odigosPage = new OdigosPage(driver, wait);
    }

    @Test(priority = 1, description = 'it should be verify login user')
    void testVerifyUser() {
        hydraPage.initialize();
        hydraPage.performLogin('Singha Proxy', 'Singha');
        odigosPage.initialize();
        odigosPage.verifyUser();
    }  

    @Test(priority=2, description='it should be check required field "Source"')
    void testRequiredSource(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.openForm();
        odigosPage.inputForm('','http://www.tangotidende.no','P');
        odigosPage.chkRequiredSource();        
    }

    @Test(priority=3, description='it should be check validation of field "Source"')
    void testValidationSource(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.openForm();
        odigosPage.inputForm('12345','http://www.tangotidende.no','P');
        odigosPage.chkValidatedSource();      
    }

    @Test(priority=4, description='it should be check required field "Destination"')
    void testRequiredDestination(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.openForm();
        odigosPage.inputForm('/test','','P');
        odigosPage.chkRequiredDestination();        
    }   

    @Test(priority=5, description='it should be check validation of field "Destination" ')
    void testValidationDestination(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.openForm();
        odigosPage.inputForm('/test','12345','P');
        odigosPage.chkValidatedDestination();  
    }

    @Test(priority=6, description='it should be check required field "Type"')
    void testRequiredType(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.openForm();
        odigosPage.inputForm('/test','http://www.tangotidende.no','');
        odigosPage.chkRequiredType();
    }

    @Test(priority=7, description='it should be add rule for publication')
    void testAddRule(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        def ruleCount = odigosPage.getOdigosRow();
        odigosPage.openForm();          
        odigosPage.inputForm('/testRule','http://www.tangotidende.no/tv','P');
        odigosPage.addRule(ruleCount);
    }   

    @Test(priority=8, description='it should be cancel to update rule for application')
    void testCancelUpdateRule(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.cancelUpdateRule();
    }

    @Test(priority=9, description='it should be update rule')
    void testUpdateRule(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.selectUpdate();
        odigosPage.inputForm('/updateRule','http://www.tangotidende.no/update','C');
        odigosPage.chkUpdateRule('/updateRule','http://www.tangotidende.no/update','C');
    }
    
    @Test(priority=10, description='it should be cancel to remove rule from publication')
    void testCancelRemoveRule(){
        odigosPage.callAppList();       
        odigosPage.selectPublication();
        def ruleCount = odigosPage.getOdigosRow();
        odigosPage.cancelRemoveRule(ruleCount);
    }

    @Test(priority=11, description='it should be remove rule from publication')
    void testRemoveRule(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        def ruleCount = odigosPage.getOdigosRow();
        odigosPage.removeRule(ruleCount);
    }

    @Test(priority=12,description='it should be uncheck all column')
    void testUncheckAll(){
        odigosPage.callAppList();
        odigosPage.selectPublication();
        odigosPage.uncheckAll();
    }

    @AfterClass
    void tearDown() {
        odigosPage.logout();
    }
}