import static ch.qos.logback.classic.Level.INFO
import static ch.qos.logback.classic.Level.DEBUG

import ch.qos.logback.classic.encoder.PatternLayoutEncoder
import ch.qos.logback.core.ConsoleAppender

appender("CONSOLE", ConsoleAppender) {
    withJansi = true
    encoder(PatternLayoutEncoder) {
        pattern = "%-25(%d{HH:mm:ss.SSS} %boldMagenta(%-5level)) %cyan(%logger{32}) - %msg%n"
    }
}
root(INFO, ["CONSOLE"])
