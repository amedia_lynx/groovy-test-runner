# README #

This is a Groovy test runner, which makes use of asynchronous Selenium API behind the scene. We opt for Groovy for a more relaxed syntax, as we can skip strong-typing of Java.

To aid test automation for AngularJS applications, we have "borrowed" (i.e., stole and modified) some of the JS scripts from Protractor. Thus we can also enjoy Angular-specific selectors like Protractor does. The script resides at `src/test/resources/clientsidescripts.js`. See below for the list of supported locators:

* [WebDriver Locators](https://selenium.googlecode.com/git/docs/api/java/org/openqa/selenium/By.html)
* [Protractor Locators](https://angular.github.io/protractor/#/api?view=ProtractorBy)

We also make use of `PageObject` design pattern, and add a set of helper methods to make many API calls almost indistinguishable from Protractor script. The end result is that more than 90% of Protractor code can be copied and pasted into Groovy. So the migration path is rather seamless.

## To run the test ###

* By default, the project is set to test against Firefox browser.
* To run, simply call `mvn clean test`.

## Miscellaneous Notes ###

* The framework is setup at log level `INFO`. But you may modify log level at `src/test/resources/logback.groovy` if you want finer-grained debugging messages
* If you want to test against a different browser, or even to `Remote WebDriver` (i.e, connecting to an existing Selenium server), you may change configuration here: `src/test/resources/config.json`.
* To create new Test class, make sure that such test extends from `BaseTest`, and that such test is defined inside `src/test/resources/testng.xml`.
* Each PageObject you create must extend from `BasePage`.